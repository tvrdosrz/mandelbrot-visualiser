VERSION = 0.0

PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

CFLAGS = `pkg-config --cflags gtk4`
LDFLAGS = `pkg-config --libs gtk4` -lm

CC = mpicc

SRC = visor.c
OBJ = ${SRC:.c=.o}

all: visor

.c.o:
	$(CC) -c $(CFLAGS) $<

visor: $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

dist: clean
	mkdir -p visor-$(VERSION)
	cp -R Makefile $(SRC) visor-$(VERSION) # Add other files once repo changes
	tar -cf - visor-$(VERSION) | gzip > visor-$(VERSION).tar.gz
	rm -rf visor-$(VERSION)

clean:
	rm -f visor $(OBJ)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f visor $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/visor
	#mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	#chmod 644 $(DESTDIR)$(MANPREFIX)/man1/visor.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/visor#\
		#$(DESTDIR)$(MANPREFIX)/man1/visor.1

.PHONY: all options dist clean install uninstall
