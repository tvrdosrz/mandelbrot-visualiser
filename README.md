# Mandelbrot visualiser
A simple GTK application for drawing the mandelbrot set, meant to showcase some features of OpenMPI.

This was written for a faculty project, and I will likely rewrite it to use pthreads soon.
